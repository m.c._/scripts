#! /bin/dash
# checks for operating system

case `uname` in
    Linux )
        LINUX=1
        os="linux"
    ;;
    Darwin )
        DARWIN=1
        os="macOS"
    ;;
esac

sum=0
var=0
path1=$(pwd)
echo "Dir:" $path1

for X in $path1/*
do
	if [ -f $X ]; then
		[ -x $X ] && check="y" || check="n"
	
		[ $check = "y" ] && var=1 || var=0
	fi
	sum=`expr $sum + $var`	
done

cd
echo ""
echo "This script will try to open now all script files in this folder!"
echo "Do you wish to continue? (y/n): "
read answ

if [ $answ = "y" ] || [ $answ = "Y" ] && [ "$(command -v code)" ]; then
    code $path1/
elif [ $answ = "y" ] || [ $answ = "Y" ] && [ "$(command -v nano)" ]; then
    for Y in $path1/*
    do
        if [ -f $Y ]; then
            nano $Y
        fi
    done
elif [ $answ = "y" ] || [ $answ = "Y" ] && [ "$(command -v vim)" ]; then
    for Y in $path1/*
    do
        if [ -f $Y ]; then
            vim $Y
        fi
    done
elif [ $answ = "y" ] || [ $answ = "Y" ] && [ "$(command -v vi)" ]; then
    for Y in $path1/*
    do
        if [ -f $Y ]; then
            vi $Y
        fi
    done    
elif [ $answ = "n" ] || [ $answ = "N" ]; then
    echo "You selected no!"
else
    echo "Invalid answer nothing will be done!"
fi
