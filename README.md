# Storing all my script examples in here.

The install.sh will try to open a text editor, so you can adjust the scripts to your needing.

Most of the script are written to pull and push data to a server.
Others are mac related workflow.

In the future i will add also linux related scripts.

# Install

To make the install.sh executable run this command:

```shell
chmod u+x install.sh
```

Then you can run the script like this:
```shell
./install.sh
```